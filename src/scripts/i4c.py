#!/usr/bin/env python
import sys
import os
import requests
import logging
import logging.config
from datetime import datetime

from argparse import ArgumentParser
from insights4ci.datasources import gitlab

from insights4ci.client.api import Insights4CIClient, Project, Pipeline, Job

SUPPORTED_BACKENDS = {'gitlab': gitlab}

DEFAULT_I4C_ADDRESS = "http://localhost:8000"

logging.config.fileConfig('logging.conf')
LOG = logging.getLogger('insights4ci.client.cli')


def get_client():
    """Initializes and returns an Insights4CIClient.

    It takes into account the value of the environment variable
    I4C_ADDRESS.
    """
    return Insights4CIClient(os.environ.get('I4C_ADDRESS',
                                            DEFAULT_I4C_ADDRESS))


def register_project(args):
    """Submit a new project.

    This should be executed only by admins on approved projects and only once
    otherwise an exception it will be raised telling this project is already
    registered.
    """

    data = vars(args)
    data.pop('func')

    client = get_client()
    try:
        project = Project(data).post(client)
    except requests.exceptions.HTTPError as ex:
        LOG.error(str(ex))
        LOG.error("Failed to register the project. Exiting...")
        return 1

    LOG.info(f"Project {data.get('name')} created:")
    for key, value in project.as_dict().items():
        LOG.info(f"{key}: {value}")


def import_jobs(args):
    try:
        backend = SUPPORTED_BACKENDS[args.backend]
    except KeyError:
        msg = ("Backend not supported. For now, only 'gitlab' "
               "is supported.")
        LOG.error(msg)

    client = get_client()
    try:
        project = Project.get_by_id(client, args.internal_id)
    # TODO: client api should raise its own Exception
    except requests.exceptions.HTTPError as ex:
        LOG.error(str(ex))
        LOG.error("Failed to get project from Insights4CI! Exiting...")
        return 1

    LOG.info(f"Fetching project ({args.external_id}) from {args.backend}...")
    external_id = args.external_id
    if args.since:
        try:
            date_from = datetime.strptime(args.since, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            try:
                date_from = datetime.strptime(args.since, '%Y-%m-%d')
            except ValueError:
                LOG.info(f"Can not reade date: {args.since} the prefered format"
                         "is `%Y-%m-%d` or `%Y-%m-%dT%H:%M:%S`")
                return []
        g_pipelines = backend.DataSource.fetch_pipelines_since(external_id, date_from)
    else:
        try:
            g_pipelines = backend.DataSource.fetch_pipelines_since(
                external_id,
                project.get_latest_pipeline(client).created_at
            )
        except requests.exceptions.HTTPError:
            LOG.info("Project {project.name} desn't have any piplines, fetching latest"
                     " pipelines form {external_id}")
            g_pipelines = backend.DataSource.fetch_pipelines_since(external_id)

    g_pipelines.sort(key=lambda p: p.created_at)
    for g_pipeline in g_pipelines:
        # Creates a local pipeline object and submmit it.
        try:
            pipeline = Pipeline(project, g_pipeline.as_dict()).post(client)
        except requests.exceptions.HTTPError:
            LOG.info("Pipeline {g_pipeline.external_id} already exists.")
            continue
        g_jobs = g_pipeline.jobs
        g_jobs.sort(key=lambda j: j.created_at)
        for g_job in g_jobs:
            # Creates a local job object and submmit it.
            # A job here, has already its runner and test results.
            if args.include_empty_jobs or g_job.test_results:
                Job(pipeline, g_job.as_dict()).post(client)


def main(argv=sys.argv[1:]):
    epilog = ("This CLI is in beta stage and available just for basic "
              "operations. Please use it as reference for your own use "
              "case or visit the REST API.")
    parser = ArgumentParser(description="insights4ci basic CLI.",
                            epilog=epilog)

    subparsers = parser.add_subparsers(help='sub-command help')

    # projects-register
    help_msg = ("Register a new project.")
    projects = subparsers.add_parser("projects-register",
                                     help=help_msg)
    projects.add_argument('--name',
                          required=True,
                          help="Project's name.")
    projects.add_argument('--description',
                          help="Project's description.")
    projects.add_argument('--repository-url',
                          help="Source code repository.")
    projects.set_defaults(func=register_project)

    # jobs-import
    help_msg = ("Import project data from external backend. This will fetch "
                "all jobs data found. The 'source' field for each pipeline "
                "found it will be properly set depending on the backend used")
    jobs = subparsers.add_parser("jobs-import",
                                 help=help_msg)
    jobs.add_argument('--external_id',
                      required=True,
                      type=str,
                      help="Project's external id on that backend.")
    jobs.add_argument('--since',
                      required=False,
                      type=str,
                      help="Fetches pipelines data available since a specific date. "
                      "When this is not set, it will fetch all pipelines since the last "
                      "fetch. Be careful if you have not executed it for a while. The "
                      "date has to be in format: %%Y-%%m-%%dT%%H:%%M:%%S")
    jobs.add_argument('--internal_id',
                      required=True,
                      type=str,
                      help="Project's internal id on Insights4CI.")
    jobs.add_argument('--backend',
                      default="gitlab",
                      choices=SUPPORTED_BACKENDS.keys(),
                      help="Fetching data from backend. Default: gitlab.")
    jobs.add_argument('--include_empty_jobs',
                      type=bool,
                      default=False,
                      help=("Whether to include jobs that don't contain "
                            "any test results"))
    jobs.set_defaults(func=import_jobs)

    args = parser.parse_args(argv)
    if hasattr(args, 'func'):
        return args.func(args)
    else:
        parser.print_help()
        return 0


if __name__ == "__main__":
    sys.exit(main())
